const bcrypt = require('bcrypt');

const {User} = require('../models/userModel');

const getUser = async (req, res) => {
  // eslint-disable-next-line max-len
  const user = await User.findOne({_id: req.user._id}, {password: 0, __v: 0});
  if (!user) {
    res.status(400).json({message: 'There is no such user'});
  }

  res.json({user});
};

const deleteUser = async (req, res) => {
  const user = await User.findOne({_id: req.user._id});
  if (!user) {
    res.status(400).json({message: 'There is no such user'});
  }
  await User.deleteOne({_id: req.user._id});

  res.json({message: 'User was deleted successfully'});
};

const changeUserPassword = async (req, res) => {
  const {oldPassword, newPassword} = req.body;
  const user = await User.findOne({_id: req.user._id});

  if (!user) {
    res.status(400).json({message: 'There is no such user'});
  }
  if (oldPassword && newPassword) {
    if (!(await bcrypt.compare(oldPassword, user.password))) {
      res.status(400).json({message: 'Previous password is not correct'});
    } else {
      if (oldPassword !== newPassword) {
        user.password = await bcrypt.hash(newPassword, 10);
        await user.save();
        res.json({message: 'Password was changed'});
      } else {
        res.status(400).json({message: 'It must be a different password'});
      }
    }
  } else {
    res.status(400).json({message: 'You must write the both passwords'});
  }
};

module.exports = {
  getUser,
  deleteUser,
  changeUserPassword,
};
