module.exports = {
  PASS_REGEXP: new RegExp(process.env.PASS_REGEXP) || '^[a-zA-Z0-9]{6,24}$',
  JWT_SECRET: process.env.JWT_SECRET || 'secret',
};
