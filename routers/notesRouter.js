const express = require('express');
const router = new express.Router();

const {wrapper} = require('../helpers');
const {checkToken} = require('../middlewares/tokenMiddleware');
const {
  addNote,
  getNotes,
  getNoteById,
  updateNoteById,
  deleteNoteById,
  patchNoteById,
} = require('../controllers/notesController');

router.get('/', wrapper(checkToken), wrapper(getNotes));
router.post('/', wrapper(checkToken), wrapper(addNote));
router.get('/:id', wrapper(checkToken), wrapper(getNoteById));
router.put('/:id', wrapper(checkToken), wrapper(updateNoteById));
router.delete('/:id', wrapper(checkToken), wrapper(deleteNoteById));
router.patch('/:id', wrapper(checkToken), wrapper(patchNoteById));

module.exports = router;
