const Joi = require('joi');

const {PASS_REGEXP} = require('../config');

module.exports.checkAuth = async (req, res, next) => {
  const schema = Joi.object({
    username: Joi.string()
        .alphanum()
        .min(3)
        .max(30)
        .required(),
    password: Joi.string()
        .pattern(PASS_REGEXP)
        .required(),
  });

  try {
    await schema.validateAsync(req.body);
  } catch (err) {
    res.status(400).json({message: err.message});
  }
  next();
};
