const {User} = require('../models/userModel');
const {Note} = require('../models/noteModel');

const addNote = async (req, res) => {
  const user = await User.findOne({_id: req.user._id});
  const {text} = req.body;
  if (!text) {
    res.status(400).json({message: 'The text field is empty'});
  }
  const note = new Note({
    userId: user._id,
    text,
  });
  await note.save();

  res.json({message: 'Note added successfully'});
};

const getNotes = async (req, res) => {
  const {skip = '0', limit = '0'} = req.query;
  const requestOptions = {
    skip: parseInt(skip),
    limit: limit > 100 ? 5 : parseInt(limit),
  };

  const user = await User.findOne({_id: req.user._id});
  const notes = await Note.find(
      {userId: user._id}, {__v: 0}, requestOptions,
  );

  res.json({notes});
};

const getNoteById = async (req, res) => {
  const note = await Note.findOne(
      {userId: req.user._id, _id: req.params['id']}, {__v: 0},
  );
  if (!note) {
    res.status(400).json({message: 'TThere is no such note'});
  }

  res.json({note});
};

const updateNoteById = async (req, res) => {
  const note = await Note.findOne(
      {userId: req.user._id, _id: req.params['id']}, {__v: 0},
  );

  if (!note) {
    res.status(400).json({message: 'There is no such note'});
  }

  if (!req.body['text']) {
    res.status(400).json({message: 'Missing text field'});
  }

  await Note.updateOne(note, req.body);

  res.json({message: 'The note was updated successfully'});
};

const deleteNoteById = async (req, res) => {
  const note = await Note.findOne(
      {userId: req.user._id, _id: req.params['id']}, {__v: 0},
  );

  if (!note) {
    res.status(400).json({message: 'There is no such note'});
  }

  await Note.deleteOne(note);

  res.json({message: 'The note was deleted successfully'});
};

const patchNoteById = async (req, res) => {
  const note = await Note.findOne(
      {userId: req.user._id, _id: req.params['id']}, {__v: 0},
  );
  if (!note) {
    res.status(400).json({message: 'There is no such note'});
  }

  await Note.updateOne(note, {completed: !note['completed']});

  res.json({message: 'The note was changed successfully'});
};

module.exports = {
  addNote,
  getNotes,
  getNoteById,
  updateNoteById,
  deleteNoteById,
  patchNoteById,
};
