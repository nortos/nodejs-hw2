const express = require('express');
const router = new express.Router();

const {wrapper} = require('../helpers');
const {checkAuth} = require('../middlewares/authMiddleware');
const {register, login} = require('../controllers/authController');

router.post('/register', wrapper(checkAuth), wrapper(register));
router.post('/login', wrapper(login));

module.exports = router;
