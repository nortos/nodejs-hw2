const express = require('express');
const router = new express.Router();

const {wrapper} = require('../helpers');
const {checkToken} = require('../middlewares/tokenMiddleware');
const {
  getUser,
  deleteUser,
  changeUserPassword,
} = require('../controllers/usersController');

router.get('/me', wrapper(checkToken), wrapper(getUser));
router.delete('/me', wrapper(checkToken), wrapper(deleteUser));
router.patch('/me', wrapper(checkToken), wrapper(changeUserPassword));

module.exports = router;
